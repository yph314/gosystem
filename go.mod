module gotool

go 1.17

require(
	gotool/systemtool v0.0.0
)

replace (
    gotool/systemtool => ../systemtool
)
require (
	github.com/guptarohit/asciigraph v0.5.3
	github.com/liushuochen/gotable v0.0.0-20220422082030-b90637e95040
	github.com/pterm/pterm v0.12.41
	github.com/shirou/gopsutil/v3 v3.22.3
)

require (
	github.com/atomicgo/cursor v0.0.1 // indirect
	github.com/go-ole/go-ole v1.2.6 // indirect
	github.com/gookit/color v1.5.0 // indirect
	github.com/lufia/plan9stats v0.0.0-20211012122336-39d0f177ccd0 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/power-devops/perfstat v0.0.0-20210106213030-5aafc221ea8c // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/tklauser/go-sysconf v0.3.10 // indirect
	github.com/tklauser/numcpus v0.4.0 // indirect
	github.com/xo/terminfo v0.0.0-20210125001918-ca9a967f8778 // indirect
	github.com/yusufpapurcu/wmi v1.2.2 // indirect
	golang.org/x/sys v0.0.0-20220319134239-a9b59b0215f8 // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
)
