package systemtool

import (
	"github.com/shirou/gopsutil/v3/mem"
)

func GetMem() (*mem.VirtualMemoryStat, error) {
	v, err := mem.VirtualMemory()
	return v, err
}
