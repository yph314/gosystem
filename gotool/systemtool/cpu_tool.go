package systemtool

import (
	"context"
	"time"

	"github.com/shirou/gopsutil/v3/cpu"
)

func GetCpu(interval int) ([]float64, error) {
	// info, err := cpu.Info()
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// fmt.Println(info)

	return cpu.PercentWithContext(context.Background(), time.Millisecond*time.Duration(interval), false)
}
